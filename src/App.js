import {  Route, Routes, BrowserRouter  } from "react-router-dom";
import "./App.css";
import React from "react";
import Home from "./Home";
import TeamsDetails from "./teamsDetails";
import PageNotFound from "./PageNotFind";


const App = () => {
 return (
   <BrowserRouter>
   <Routes>
   <Route exact path="/" element={<Home/>} />
     <Route exact path="/team/:teamId" element={<TeamsDetails />} />
     <Route exact path="/*" element={<PageNotFound />} />
   </Routes>
 </BrowserRouter>
 );
};
export default App;
