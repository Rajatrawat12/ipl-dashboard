import React, { useEffect, useState } from "react";
import RecentMatches from "./recentMatches";
import "./teamsDetails.css";
import { TailSpin } from "react-loader-spinner";
import { useParams } from "react-router-dom";;


const TeamsDetails = () => {
  const [teamData, setTeamData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const { teamId } = useParams();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`https://apis.ccbp.in/ipl/${teamId}`);
        console.log("Response", response);
        if (response.ok) {
          const jsonData = await response.json();
          setTeamData(jsonData);
        } else {
          throw new Error("Failed to fetch team data");
        }
      } catch (error) {
        console.log("Error fetching data:", error);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
  }, [teamId]);

  return (
    <div className="body">
      {isLoading ? (
        <TailSpin color="#00BFFF" height={50} width={50} />
      ) : (
        <>
          {teamData && (
            <div className="teambannerUrl">
              <img
                className="TeamBannerImage"
                src={teamData.team_banner_url}
                alt="Team Banner"
              />
            </div>
          )}
          <p className="latestMatchText">Latest Matches</p>
          <div className="recantMatchDetailContainer">
            <div className="recantmatchDetail">
              {teamData && teamData.latest_match_details && (
                <div className="latestMatchInternalContainer">
                  <h1 className="recantTeamMatchPlayedDetailsStartHeading">
                    {teamData.latest_match_details.competing_team}
                  </h1>
                  <p className="recantTeamMatchPlayedDetailsStart">
                    {teamData.latest_match_details.date}
                  </p>
                  <p className="recantTeamMatchPlayedDetailsStart">
                    {teamData.latest_match_details.venue}
                  </p>
                  <p className="recantTeamMatchPlayedDetailsStart">
                    {teamData.latest_match_details.result}
                  </p>
                </div>
              )}
            </div>

            {teamData && teamData.latest_match_details && (
              <div className="latestMatchInternalContainer">
                <img
                  className="recentTeamLogo"
                  src={teamData.latest_match_details.competing_team_logo}
                  alt="Competing Team Logo"
                />
              </div>
            )}
            {teamData && teamData.latest_match_details && (
              <div className="latestMatchInternalContainer">
                <p className="recantTeamMatchPlayedDetailsEnd">First Innings</p>
                <p className="recantTeamMatchPlayedDetailsEnd">
                  {teamData.latest_match_details.first_innings}
                </p>
                <p className="recantTeamMatchPlayedDetailsEnd">Second Innings</p>
                <p className="recantTeamMatchPlayedDetailsEnd">
                  {teamData.latest_match_details.second_innings}
                </p>
                <p className="recantTeamMatchPlayedDetailsEnd">Man Of The Match</p>
                <p className="recantTeamMatchPlayedDetailsEnd">
                  {teamData.latest_match_details.man_of_the_match}
                </p>
                <p className="recantTeamMatchPlayedDetailsEnd">Umpires</p>
                <p className="recantTeamMatchPlayedDetailsEnd">
                  {teamData.latest_match_details.umpires}
                </p>
              </div>
            )}
          </div>
          <div className="matchesResultDetails">
            {teamData &&
              teamData.recent_matches.map((team) => (
                <RecentMatches key={team.id} team={team} />
              ))}
          </div>
        </>
      )}
    </div>
  );
};

export default TeamsDetails;

