import "./Home.css";
import React, { useEffect, useState } from "react";
import {TailSpin} from "react-loader-spinner";
import { Link } from "react-router-dom";



const Home = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch("https://apis.ccbp.in/ipl");
      const jsonData = await response.json();
      setData(jsonData);
      setIsLoading(false); // Set isLoading to false once data is fetched
    } catch (error) {
      console.log("Error fetching data:", error);
    }
  };

  return (
    <div className="dashboardContainer">
      <div className="iplLogoContainer">
        <img
          className="iplLogo"
          src="https://assets.ccbp.in/frontend/react-js/ipl-logo-img.png"
          alt="IPL Logo"
        />
        <h1 className="iplDashboardText">IPL Dashboard</h1>
      </div>
      {isLoading ? (
        <div className="loaderContainer">
          <TailSpin color="#00BFFF" height={50} width={50} />
        </div>
      ) : (
        <div className="teamsContainer">     
            {data.teams.map((team) => ( 
             <Link key={team.id} to={`/team/${team.id}`}>
                <div className="teamNamesContainer" key={team.id}>
                  <img className="teamLogo" src={team.team_image_url} alt="Team Logo" />
                  <p className="teamName">{team.name}</p>
                </div>
                </Link>
            
            ))}
            
        </div>
      )}
    </div>
  );
};

export default Home;
