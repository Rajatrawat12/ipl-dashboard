import React from "react";
import "./recentMatches.css";
const RecentMatches = (props) => {
  const { team} = props;
  const {competing_team_logo, match_status, competing_team, result } = team;

  return (
      <div className="opponentTeamcontainer">
        <img className="competingTeamLogo" src={competing_team_logo}  alt="Competing team"/>
        <h1 className="competingTeamHeading">{competing_team}</h1>
        <p className="competingTeam">{result}</p>
        <p className={match_status === "Won" ? "won" : "lost"}>
          {match_status}
        </p>
      </div>
  );
};
export default RecentMatches;
